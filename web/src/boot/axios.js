import Vue from "vue";
import axios from "axios";
import { Cookies as cookie, Loading, Notify, Dialog } from "quasar";

const instance = axios.create({
  baseURL: process.env.DEV
    ? "http://192.168.43.15:1337"
    : "https://192.168.43.15:1337",
  // withCredentials: false,
  headers: {
    Authorization: "Bearer: " + cookie.get("jwt"),
    "User-Role": "agent"
  }
});

// Add a request interceptor
instance.interceptors.request.use(
  config => {
    // Do something before request is sent
    Loading.show();
    return config;
  },
  error => {
    // Loading.hide();
    // Do something with request error
    return Promise.reject(error);
  }
);

//  Add a response interceptor
instance.interceptors.response.use(
  response => {
    Loading.hide();
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  error => {
    if (error && typeof error.response?.status === "undefined") {
      Dialog.create({
        title: "Offline!",
        message:
          "You seem to be offline, please turn on your data and try again!"
      });
    }
    Loading.hide();

    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);

Vue.prototype.$axios = instance;
window.$axios = instance;
