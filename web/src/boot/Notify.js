import { Notify } from 'quasar'

Notify.registerType('add-notify', {
  icon: 'announcement',
  progress: true,
  color: 'positive',
  textColor: 'white',
  //   caption: `It overrides the type's default icon and color.`,
  message:'Added successfully',
  classes: 'glossy'
})