const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/Index.vue") },
      { path: "/b", component: () => import("pages/b") }
    ]
  },
  //
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "/upload-toral",
        name: "upload-toral",
        component: () => import("pages/Toral/uploadToral")
      },
      {
        path: "/torals",
        name: "torals",
        component: () => import("pages/Toral/torals")
      },
      {
        path: "/toral/:toralID",
        name: "ViewToral",
        component: () => import("pages/Toral/_viewToral.vue")
      },
      {
        path: "/toral/:editID",
        name: "edit",
        component: () => import("pages/Toral/_editToral.vue")
      },
      {
        path: "/blessing",
        name: "blessing",
        component: () => import("pages/Blessings")
      },
      {
        path: "/contact",
        name: "contact",
        component: () => import("pages/Contact")
      },
      {
        path: "/event",
        name: "event",
        component: () => import("pages/Event")
      },
      {
        path: "/calender",
        name: "calender",
        component: () => import("pages/Calender")
      }
    ]
  },
  // #HOLIDAYS
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "holiday", component: () => import("pages/Holidays/index.vue") },
      { path: "/hanukah", component: () => import("pages/Holidays/hanukah") },
      { path: "/passover", component: () => import("pages/Holidays/pesach") },
      {
        path: "/atonement",
        component: () => import("pages/Holidays/atonement")
      },
      {
        path: "/purim",
        component: () => import("pages/Holidays/purim")
      },
      {
        path: "/terbanacle",
        component: () => import("pages/Holidays/terbanacle")
      },

      {
        path: "/shabbath",
        component: () => import("pages/Holidays/shabbath")
      },
      {
        path: "/first-fruit",
        component: () => import("pages/Holidays/FirstFruit")
      },
      {
        path: "/Simchat-torah",
        component: () => import("pages/Holidays/SimchatTorah")
      },

      {
        path: "/Rosh-hashanah",
        component: () => import("pages/Holidays/RoshHashanah")
      },
      {
        path: "/passover-rules",
        component: () => import("pages/Holidays/RulesOfPassover")
      },
      {
        path: "/hanukah_blessing",
        component: () => import("pages/Holidays/hanukahBlessing")
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue")
  }
];

export default routes;
