import { Cookies, Notify, Loading } from "quasar";

const Toral = {
  namespaced: false,

  state: {
    torals: []
  },

  mutations: {
    PUSH_TORAL(state, $toral) {
      state.torals.unshift($toral);
    },

    SET_TORALS(state, $toral) {
      state.torals = $toral;
    }
  },

  actions: {
    async getTorals({ commit }) {
      let { data } = await $axios.get("/get-all");
      if (data) {
        commit("SET_TORALS", data);
      }
    },

    async ADD_TORAL({ commit }, $toralData) {
      let { topic, toral, halftara } = $toralData;
      try {
        let data = await $axios.post("/toral", {
          topic,
          toral,
          halftara
        });
        if (data) {
          commit("PUSH_TORAL", data);
          Notify.create({
            message: "your toral portion have successfully added",
            color: "positive"
          });

          this.$router.push("/torals");
        }
      } catch ({ response }) {
        if (response && response.status !== 200) {
          Notify.create({
            color: "red",
            message: "Sorry, an unexpected error occured"
          });
        }
      }
    },

    async EDIT_TORAL({ commit, state }, toralData) {
      try {
        let data = await $axios.patch("/update-toral", toralData);
        if (data) {
          let toralIndex = state.torals.findIndex(toral => toral.id == data.id);
          state.torals.splice(toralIndex, 1, data);

          Notify.create({
            color: "positive",
            message: "toral has been updated successfully!"
          });
        }
      } catch (e) {
        Notify.create({
          color: "negative",
          message: "Sorry, an error occured while trying to update this toral"
        });
      }
    }
  },
  getters: {
    torals(state) {
      return state.torals;
    },
    getToral: state => toralId => {
      return state.torals.find(toral => toral.id == toralId);
    }

    // getLodge: state => lodgeId => {
    //   return state.lodges.find(lodge => lodge.id == lodgeId);
    // }
  }
};
export default Toral;
