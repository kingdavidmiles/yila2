module.exports = {
  friendlyName: "Update toral",

  description: "updating every single toral by their ids",

  inputs: {
    id: {
      type: "string",
      required: true,
    },
    topic: {
      type: "string",
      required: true,
      description: "topic of the toral you want to upload to the site",
    },
    toral: {
      type: "string",
      required: true,
      description: "toral or the bible portion you want upload",
      example: "genesis 1-2",
    },

    halftara: {
      type: "string",
      required: true,
      description: "a sconde portion of bible to read after reading toral",
      example: "ezekiel 20:4",
    },
  },

  exits: {},

  fn: async function (inputs) {
    var updateToral = await Toral.updateOne({id:inputs.id}).set(inputs);


    // All done.
    return updateToral;
  },
};
