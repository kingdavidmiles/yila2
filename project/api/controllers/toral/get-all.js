module.exports = {
  friendlyName: "Geting all",

  description: "Torals toral ",

  inputs: {},

  exits: {
    success: {
      statusCode: 200,
      description: "All Toral has been returned back to the client",
    },
  },

  fn: async function (inputs) {
    var torals = await Toral.find()
      .sort("createdAt DESC")
      .limit(1)
      .populateAll();

    // All done.
    return torals;
  },
};
