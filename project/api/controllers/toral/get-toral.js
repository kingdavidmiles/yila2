module.exports = {
  friendlyName: "Get toral",

  description: "getting a every single toral",

  inputs: {
    id: {
      type: "string",
      required: true,
    },
  },

  exits: {},

  fn: async function (inputs) {
    var getToral = await Toral.find({ id: inputs.id });
    // All done.
    return getToral;
  },
};
