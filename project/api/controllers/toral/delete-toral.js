module.exports = {
  friendlyName: "Delete toral",

  description: "deleting a toral from database",

  inputs: {
    id: {
      type: "string",
      required: true,
    },
  },

  exits: {},

  fn: async function (inputs) {
    var deleteToral = await Toral.destroy({ id: inputs.id });
    // All done.
    return deleteToral;
  },
};
