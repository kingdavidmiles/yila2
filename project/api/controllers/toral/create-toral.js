module.exports = {
  friendlyName: "Create Toral",

  description: "A toral or bible portion to read every week",

  inputs: {
    topic: {
      type: "string",
      required: true,
      description: "topic of the toral you want to upload to the site",
    },
    toral: {
      type: "string",
      required: true,
      description: "toral or the bible portion you want upload",
      example: "genesis 1-2",
    },

    halftara: {
      type: "string",
      required: true,
      description: "a sconde portion of bible to read after reading toral",
      example: "ezekiel 20:4",
    },
  },

  exits: {
    notFound: {
      description: "The post is not recognized.",
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    var toral = await Toral.create({
      toral: inputs.toral,
      topic: inputs.topic,
      halftara: inputs.halftara,
    }).fetch();

    if (!toral) {
      throw "notFound";
    }
    this.req.session.toralId = toral.id;
    // All done.
    return toral;
  },
};
